
 # Technology stack
 
 - Java 11
 - Maven
 - Spring Boot 2.2.13
 - JCEF 107.1.9.1

 # Run app

 - start reac
    - src/main/webapp/package.json
    - run `start`
 - start app
    - src/main/java/pl/mkowalewski/jcef/application
    - run `JcefProcApplication.java default configuration`
    
 # Build and run

 `mvn clean install`
 
 `java -jar target/jcef-0.0.1-SNAPSHOT.jar`
 
 
 # Useful links
 
 - https://www.zkoss.org/wiki/Small_Talks/2022/June/Building_a_Desktop_Application_using_ZK_and_JCEF
 - https://www.zkoss.org/wiki/ZK_Installation_Guide/Quick_Start/Create_and_Run_Your_First_ZK_Application_with_Spring_Boot
 - https://plugins.jetbrains.com/docs/intellij/jcef.html
 - https://www.baeldung.com/spring-mvc-static-resources
 - https://www.youtube.com/watch?v=_CLLw3QAuOE
  