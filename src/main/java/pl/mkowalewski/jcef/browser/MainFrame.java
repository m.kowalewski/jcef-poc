package pl.mkowalewski.jcef.browser;

import me.friwi.jcefmaven.CefAppBuilder;
import me.friwi.jcefmaven.CefInitializationException;
import me.friwi.jcefmaven.UnsupportedPlatformException;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class MainFrame extends JFrame {

    private final CefApp cefApp_;
    private final CefClient client_;
    private final CefBrowser browser_;
    private final Component browerUI_;

    public MainFrame(String startURL, boolean useOSR, boolean isTransparent) throws HeadlessException, InterruptedException, UnsupportedPlatformException, CefInitializationException, IOException {

        //Create a new CefAppBuilder instance
        CefAppBuilder cefBuilder = new CefAppBuilder();
        //Configure the builder instance
        cefBuilder.setInstallDir(new File("jcef-bundle"));

        cefApp_ = cefBuilder.build();
        client_ = cefApp_.createClient();

        browser_ = client_.createBrowser(startURL, useOSR, isTransparent);
        browerUI_ = browser_.getUIComponent();

        getContentPane().add(browerUI_, BorderLayout.CENTER);
        pack();
        setSize(800, 600);
        setVisible(true);
        toFront();
        repaint();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                CefApp.getInstance().dispose();
                dispose();
            }
        });

    }
}
