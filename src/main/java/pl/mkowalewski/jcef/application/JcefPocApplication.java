package pl.mkowalewski.jcef.application;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import pl.mkowalewski.jcef.browser.MainFrame;

@SpringBootApplication
public class JcefPocApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder springBuilder = new SpringApplicationBuilder(JcefPocApplication.class);
        springBuilder.headless(false);
        ConfigurableApplicationContext context = springBuilder.run(args);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    new MainFrame("http://localhost:8080", false, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

}
